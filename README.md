## SimpleNotes

### Description
Microservices pet-project - user notes service.

Create/Read UI operations are present for now.

### Tech stack
+ Java 11
+ Spring Boot
+ Spring Data JPA
+ OAuth 2.0
+ PostgreSQL
+ HTML + JQuery
+ Gradle
+ Docker (Jib used to omit Dockerfile)

Auth providers:

+ Google

### How to build images
+ Ensure Docker is running
+ Ensure host in properties `spring.datasource.url` and `app.note.api.url`


    `gradlew jibDockerBuild`