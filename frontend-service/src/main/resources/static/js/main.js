$.ajaxSetup({
    beforeSend : function(xhr, settings) {
        if (settings.type == 'POST' || settings.type == 'PUT' || settings.type == 'DELETE') {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-XSRF-TOKEN",
                  Cookies.get('XSRF-TOKEN'));
            }
        }
    }
});

$.get("/app/user", function(data) {
        $("#user").html(data.name);
        $(".unauthenticated").hide();
        $(".authenticated").show();

        $.each(data.notes, function(n, elem) {
            $("#notes").append(
                "<div class=\"col-3\">" +
                    "<div class=\"card\">" +
                        "<div class=\"card-body\">" +
                            "<h5 class=\"card-title\">" + elem.title + "</h5>" +
                            "<p class=\"card-text\">" + elem.text + "</p>" +
                        "</div>" +
                    "</div>" +
                "</div>"
            );
        });
    });

$(function() {
    $('#theForm').submit(function(e) {
        var $form = $(this);
        var data = {};
        $form.serializeArray().map(function(x){data[x.name] = x.value;});

        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            contentType: "application/json",
            data: JSON.stringify(data)
        }).done(function() {
            console.log('success');
        }).fail(function() {
            console.log('fail');
        });

        e.preventDefault();
        location.reload(true);
    });
});

var logout = function() {
    $.post("/app/logout", function() {
        $("#user").html('');
        $(".unauthenticated").show();
        $(".authenticated").hide();
    })
    return true;
}

$.get("/app/error", function(data) {
    if (data) {
        $(".error").html(data);
    } else {
        $(".error").html('');
    }
});