CREATE TABLE IF NOT EXISTS user_table (
    id VARCHAR(64) NOT NULL,
    name VARCHAR(128) NOT NULL,
    email VARCHAR(128) NOT NULL,
    user_pic TEXT,
    last_visit VARCHAR(64),
    PRIMARY KEY (id));