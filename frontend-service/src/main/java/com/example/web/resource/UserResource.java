package com.example.web.resource;

import com.example.web.dto.NoteDtoList;
import com.example.web.dto.UserDto;
import com.example.web.model.User;
import com.example.web.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import javax.servlet.http.HttpSession;

@RestController
public class UserResource {
    private final NoteService noteService;

    @Autowired
    public UserResource(NoteService noteService) {
        this.noteService = noteService;
    }

    @GetMapping("/user")
    public UserDto getUser(@SessionAttribute(name = "user") User user) {
        NoteDtoList noteDtoList = noteService.listByUser(user.getId());
        return new UserDto(user.getName(), noteDtoList.getList());
    }

    @GetMapping("/error")
    public String error(HttpSession session) {
        String message = (String) session.getAttribute("error.message");
        session.removeAttribute("error.message");
        return message;
    }
}
