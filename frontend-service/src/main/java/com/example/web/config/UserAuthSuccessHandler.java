package com.example.web.config;

import com.example.web.model.User;
import com.example.web.service.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UserAuthSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
    private final UserService userService;

    public UserAuthSuccessHandler(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse resp, Authentication auth)
            throws ServletException, IOException {
        super.onAuthenticationSuccess(req, resp, auth);

        User user = userService.extractFromAuth(auth);
        userService.save(user);
        req.getSession().setAttribute("user", user);
    }
}
