package com.example.web.service.impl;

import com.example.web.model.User;
import com.example.web.repository.UserRepository;
import com.example.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.Map;

@Service
public class RepoBasedUserService implements UserService {
    private final UserRepository userRepository;

    @Autowired
    public RepoBasedUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User extractFromAuth(Authentication auth) {
        OAuth2User oAuth2User = (OAuth2User) auth.getPrincipal();
        Map<String, Object> map = oAuth2User.getAttributes();

        String id = (String) map.get("sub");

        User user = userRepository.findById(id).orElseGet(() -> {
            User userNew = new User();

            userNew.setId(id);
            userNew.setName((String) map.get("name"));
            userNew.setEmail((String) map.get("email"));
            userNew.setUserPic((String) map.get("picture"));

            return userNew;
        });

        user.setLastVisit(OffsetDateTime.now().toString());

        return user;
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }
}
