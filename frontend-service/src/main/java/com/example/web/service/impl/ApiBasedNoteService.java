package com.example.web.service.impl;

import com.example.web.dto.NoteDto;
import com.example.web.dto.NoteDtoList;
import com.example.web.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ApiBasedNoteService implements NoteService {
    @Value("${app.note.api.url}")
    private String apiUrl;

    private final RestTemplate restTemplate;

    @Autowired
    public ApiBasedNoteService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public NoteDtoList listByUser(String userId) {
        return restTemplate.getForObject(apiUrl + "?user=" + userId, NoteDtoList.class);
    }

    @Override
    public NoteDto getById(long id) {
        return restTemplate.getForObject(apiUrl + "/" + id, NoteDto.class);
    }

    @Override
    public NoteDto getByTitle(String title) {
        return restTemplate.getForObject(apiUrl + "?title=" + title, NoteDto.class);
    }

    @Override
    public void add(NoteDto note) {
        restTemplate.postForEntity(apiUrl, note, ResponseEntity.class);
    }

    @Override
    public void update(NoteDto note) {
        restTemplate.put(apiUrl, note);
    }

    @Override
    public void removeById(long id) {
        restTemplate.delete(apiUrl + "/" + id);
    }
}
