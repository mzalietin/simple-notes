package com.example.web.service;

import com.example.web.dto.NoteDto;
import com.example.web.dto.NoteDtoList;

public interface NoteService {
    NoteDtoList listByUser(String userId);
    NoteDto getById(long id);
    NoteDto getByTitle(String title);
    void add(NoteDto note);
    void update(NoteDto note);
    void removeById(long id);
}
