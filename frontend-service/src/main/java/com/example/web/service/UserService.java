package com.example.web.service;

import com.example.web.model.User;
import org.springframework.security.core.Authentication;

public interface UserService {
    User extractFromAuth(Authentication auth);
    void save(User user);
}
