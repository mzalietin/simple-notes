package com.example.web.dto;

import java.util.List;

public class NoteDtoList {
    private List<NoteDto> list;

    public List<NoteDto> getList() {
        return list;
    }

    public void setList(List<NoteDto> list) {
        this.list = list;
    }
}
