package com.example.web.dto;

import java.io.Serializable;
import java.util.List;

public class UserDto implements Serializable {
    private String name;
    private List<NoteDto> notes;

    public UserDto() {
    }

    public UserDto(String name, List<NoteDto> notes) {
        this.name = name;
        this.notes = notes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<NoteDto> getNotes() {
        return notes;
    }

    public void setNotes(List<NoteDto> notes) {
        this.notes = notes;
    }
}
