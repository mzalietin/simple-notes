package com.example.noteservice.resource;

import com.example.noteservice.NoteServiceApplication;
import com.example.noteservice.model.Note;
import com.example.noteservice.repository.NoteRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.OffsetDateTime;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = NoteServiceApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class NoteControllerIntegrationTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private NoteRepository noteRepository;

    @Test
    public void whenGetById_thenReturnsJsonDto() throws Exception {
        Note testNote = noteMock();

        testNote.setId(noteRepository.saveAndFlush(testNote).getId());

        mockMvc.perform(get("/notes/" + testNote.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(is(testNote.getId()), Long.class))
                .andExpect(jsonPath("$.title", is(testNote.getTitle())))
                .andExpect(jsonPath("$.text", is(testNote.getText())));
    }

    private Note noteMock() {
        Note noteEntity = new Note();

        noteEntity.setTitle("Test");
        noteEntity.setText("Sample integration test");
        noteEntity.setUserId("123456789");
        noteEntity.setCreated(OffsetDateTime.now().toString());

        return noteEntity;
    }
}
