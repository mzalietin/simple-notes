package com.example.noteservice.service;

import com.example.noteservice.dto.NoteDto;
import com.example.noteservice.model.Note;
import com.example.noteservice.repository.NoteRepository;
import com.example.noteservice.service.NoteService;
import com.example.noteservice.service.impl.RepoBasedNoteService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NoteServiceImplTest {
    private NoteService noteService;
    private NoteRepository noteRepository;

    @BeforeEach
    public void setUp() {
        noteRepository = mock(NoteRepository.class);
        ModelMapper modelMapper = new ModelMapper();
        noteService = new RepoBasedNoteService(noteRepository, modelMapper);
    }

    @Test
    public void whenValidId_thenNoteShouldBeFound() {
        Note note = notesMocks().get(0);
        long id = note.getId();
        when(noteRepository.findById(id)).thenReturn(Optional.of(note));

        NoteDto result = noteService.getById(id);

        assertEntityToDto(note, result);
    }

    @Test
    public void whenListByUser_thenFoundByUserId() {
        String userId = "123456789";
        List<Note> notes = notesMocks();
        when(noteRepository.findByUserId(userId)).thenReturn(notes);

        List<NoteDto> result = noteService.listByUser(userId).getList();

        result.forEach(dto -> assertEntityToDto(notes.get(result.indexOf(dto)), dto));
    }

    private void assertEntityToDto(Note note, NoteDto noteDto) {
        assertEquals(note.getId(), noteDto.getId());
        assertEquals(note.getTitle(), noteDto.getTitle());
        assertEquals(note.getText(), noteDto.getText());
    }

    private List<Note> notesMocks() {
        List<Note> notes = new ArrayList<>();

        Note noteEntity = new Note();
        noteEntity.setId(1);
        noteEntity.setTitle("Test");
        noteEntity.setText("Sample unit test of the service");
        noteEntity.setUserId("123456789");
        noteEntity.setCreated(OffsetDateTime.now().toString());
        notes.add(noteEntity);

        noteEntity = new Note();
        noteEntity.setId(2);
        noteEntity.setTitle("Test second");
        noteEntity.setText("abc abc");
        noteEntity.setUserId("123456789");
        noteEntity.setCreated(OffsetDateTime.now().toString());
        notes.add(noteEntity);

        return notes;
    }
}
