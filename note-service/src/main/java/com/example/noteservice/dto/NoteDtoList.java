package com.example.noteservice.dto;

import java.util.List;

public class NoteDtoList {
    private List<NoteDto> list;

    public NoteDtoList() {
    }

    public NoteDtoList(List<NoteDto> list) {
        this.list = list;
    }

    public List<NoteDto> getList() {
        return list;
    }

    public void setList(List<NoteDto> list) {
        this.list = list;
    }
}
