package com.example.noteservice.service;

import com.example.noteservice.dto.NoteDto;
import com.example.noteservice.dto.NoteDtoList;

public interface NoteService {
    NoteDtoList listByUser(String userId);
    NoteDto getById(long id);
    NoteDto getByTitle(String title);
    void add(NoteDto note);
    void update(NoteDto note);
    void removeById(long id);
}
