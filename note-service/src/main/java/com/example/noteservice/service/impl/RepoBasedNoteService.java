package com.example.noteservice.service.impl;

import com.example.noteservice.dto.NoteDto;
import com.example.noteservice.dto.NoteDtoList;
import com.example.noteservice.exception.NoteNotFoundException;
import com.example.noteservice.model.Note;
import com.example.noteservice.repository.NoteRepository;
import com.example.noteservice.service.NoteService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.stream.Collectors;

import static com.example.noteservice.exception.NoteNotFoundException.FOR_ID_MSG;
import static com.example.noteservice.exception.NoteNotFoundException.FOR_TITLE_MSG;

@Service
public class RepoBasedNoteService implements NoteService {
    private final NoteRepository noteRepository;
    private final ModelMapper mapper;

    @Autowired
    public RepoBasedNoteService(NoteRepository noteRepository, ModelMapper mapper) {
        this.noteRepository = noteRepository;
        this.mapper = mapper;
    }

    public NoteDtoList listByUser(String userId) {
        return new NoteDtoList(noteRepository.findByUserId(userId)
                .stream()
                .map((n -> mapper.map(n, NoteDto.class)))
                .collect(Collectors.toList())
        );
    }

    public NoteDto getByTitle(String title) {
        Note noteEntity = noteRepository.findByTitle(title)
                .orElseThrow(() -> new NoteNotFoundException(FOR_TITLE_MSG + title));

        return mapper.map(noteEntity, NoteDto.class);
    }

    public NoteDto getById(long id) {
        Note noteEntity = noteRepository.findById(id)
                .orElseThrow(() -> new NoteNotFoundException(FOR_ID_MSG + id));

        return mapper.map(noteEntity, NoteDto.class);
    }

    @Transactional(
            propagation = Propagation.REQUIRED,
            isolation = Isolation.READ_UNCOMMITTED
    )
    public void add(NoteDto note) {
        Note noteEntity = mapper.map(note, Note.class);

        noteEntity.setCreated(OffsetDateTime.now().toString());

        noteRepository.saveAndFlush(noteEntity);
    }

    @Transactional(
            propagation = Propagation.REQUIRED,
            isolation = Isolation.READ_COMMITTED
    )
    public void update(NoteDto note) {
        Note noteEntity = noteRepository.findById(note.getId())
                .orElseThrow(() -> new NoteNotFoundException(FOR_ID_MSG + note.getId()));

        mapper.map(note, noteEntity);

        noteRepository.saveAndFlush(noteEntity);
    }

    public void removeById(long id) {
        noteRepository.deleteById(id);
    }
}
