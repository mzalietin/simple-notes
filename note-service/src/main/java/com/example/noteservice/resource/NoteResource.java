package com.example.noteservice.resource;

import com.example.noteservice.dto.NoteDto;
import com.example.noteservice.dto.NoteDtoList;
import com.example.noteservice.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("notes")
public class NoteResource {
    private final NoteService noteService;

    @Autowired
    public NoteResource(NoteService noteService) {
        this.noteService = noteService;
    }

    @GetMapping(params = "user", produces = MediaType.APPLICATION_JSON_VALUE)
    public NoteDtoList getByUser(@RequestParam String user) {
        return noteService.listByUser(user);
    }

    @GetMapping(params = "title", produces = MediaType.APPLICATION_JSON_VALUE)
    public NoteDto getByTitle(@RequestParam String title) {
        return noteService.getByTitle(title);
    }

    @GetMapping(path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public NoteDto getById(@PathVariable long id) {
        return noteService.getById(id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody NoteDto note) {
        noteService.add(note);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void update(@RequestBody NoteDto note) {
        noteService.update(note);
    }

    @DeleteMapping(path = "{id}")
    public void delete(@PathVariable long id) {
        noteService.removeById(id);
    }
}
