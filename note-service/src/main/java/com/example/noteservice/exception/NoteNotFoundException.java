package com.example.noteservice.exception;

public class NoteNotFoundException extends NotFoundException {
    public static final String NOT_FOUND = "Note not found";
    public static final String FOR_ID_MSG = NOT_FOUND + " for ID: ";
    public static final String FOR_TITLE_MSG = NOT_FOUND + " for Title: ";

    public NoteNotFoundException(String message) {
        super(message);
    }
}
