CREATE TABLE IF NOT EXISTS note (
    id SERIAL NOT NULL,
    user_id VARCHAR(64) NOT NULL,
    title VARCHAR(128) NOT NULL,
    text TEXT NOT NULL,
    created VARCHAR(64),
    PRIMARY KEY (id));